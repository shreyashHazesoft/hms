@extends('frontend.main')
@section('content')
<!-- ======= Hero Section ======= -->
<section class="hero-section" id="hero">

  <div class="wave">

    <svg width="100%" height="355px" viewBox="0 0 1920 355" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
          <path d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,757 L1017.15166,757 L0,757 L0,439.134243 Z" id="Path"></path>
        </g>
      </g>
    </svg>

  </div>

  <div class="container">
    <div class="row align-items-center">

      <div class="col-12 hero-text-image">

        <div class="row">
          <div class="col-lg-8 text-center text-lg-start">
            <h1 data-aos="fade-right">Save your time by using HMS</h1>
            <p class="mb-5" data-aos="fade-right" data-aos-delay="100">
              HMS is a web application that helps you to manage the appointments, test report of the patience.
            </p>



            <p data-aos="fade-right" data-aos-delay="200" data-aos-offset="-500"><a href="#getstarted" class="btn btn-outline-white">Get started</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

</section><!-- End Hero -->
<!-- ======= Home Section ======= -->


<section class="section profession-section" id="services">
  <div class="container">
    <div class="row justify-content-center mb-2">
      <div class="col-md-12" data-aos="fade-up">
        <h2 class="section-heading">Available Services</h2>
        <hr>
      </div>
    </div>

    <div class="row d-flex">
            <div style="cursor: pointer" class="col-md-4 shadow mt-4 pt-3" data-aos="fade-up" data-aos-delay="">
                <div class="feature-1 text-center">
                    <div class="wrap-icon icon-1">
                        <img src="{{url('frontend/assets/img/appointment.png')}}" alt="" style="border-radius: 50%;" class=" img-thumbnail" width="100px" height="100px">
                    </div>
                    <h3 class="mb-3"><a href="">Book Appointment</a></h3>
                    <p>Lorem Ipsum</p>
                </div>
            </div>
          <div style="cursor: pointer" class="col-md-4 shadow mt-4 pt-3" data-aos="fade-up" data-aos-delay="">
              <div class="feature-1 text-center">
                  <div class="wrap-icon icon-1">
                      <img src="{{url('frontend/assets/img/testtube.gif')}}" alt="" style="border-radius: 50%;" class=" img-thumbnail" width="100px" height="100px">
                  </div>
                  <h3 class="mb-3"><a href="">Test</a></h3>
                  <p>Lorem Ipsum</p>
              </div>
          </div>
    </div>

  </div>
</section>

    <hr>

<!-- ======= CTA Section ======= -->
<section class="section cta-section" id="getstarted">
  <div class="container">

    <div class="row align-items-center">
      <div class="col-md-6 me-auto text-center text-md-start mb-5 mb-md-0">
        <h2 class="get-started-text">Start Using our Platform</h2>
      </div>
      <div class="col-md-5 text-center text-md-end">
        <p>
          <a href="{{url('provider-panel/login')}}" class="btn get-started-btns btn-outline-info d-inline-flex align-items-center"><i class="bi bi-people-fill"></i><span>Service Doctor</span></a>
          <a href="{{url('user-panel/login')}}" class="btn get-started-btns btn-outline-info d-inline-flex align-items-center"><i class="bi bi-people"></i><span>Service User</span></a>
        </p>
      </div>
    </div>
  </div>
</section><!-- End CTA Section -->

@endsection
